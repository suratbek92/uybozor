<?php namespace Boromir\Repair\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Model
 */
class DeviceModelCategory extends Model
{
    use Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'boromir_repair_model_categories';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $hasMany = [
        'devicemodels' => [DeviceModel::class]
    ];

    public function beforeSave()
    {
        parent::beforeSave();
        if ($this->brand_id !== $this->original['brand_id']) {
            $brand_id = $this->brand_id;
            if ($this->brand_id == null) {
                DeviceModel::where(['device_model_category_id' => $this->id])
                           ->get()
                           ->each(function ($devicemodel) {
                               $devicemodel->brand_id = null;
                               $devicemodel->save();
                           });
            } else {
                DeviceModel::where(['device_model_category_id' => $this->id])
                           ->get()
                           ->each(function ($devicemodel) use ($brand_id) {
                               $devicemodel->brand_id = $brand_id;
                               $devicemodel->save();
                           });
            }
        }

    }

}
