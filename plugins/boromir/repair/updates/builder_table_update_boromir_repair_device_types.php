<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairDeviceTypes extends Migration
{
    public function up()
    {
        Schema::rename('boromir_repair_devices', 'boromir_repair_device_types');
    }
    
    public function down()
    {
        Schema::rename('boromir_repair_device_types', 'boromir_repair_devices');
    }
}
