<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairSliders extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_sliders', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_sliders', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
