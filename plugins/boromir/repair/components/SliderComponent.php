<?php namespace Boromir\Repair\Components;

use Cms\Classes\ComponentBase;
use Boromir\Repair\Models\Slider as SliderModel;
class SliderComponent extends ComponentBase
{
    public $sliders;
    public function componentDetails()
    {
        return [
            'name'        => 'Slider Component',
            'description' => 'Render slider list'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
    public function onRun(){

        $this->sliders = $this->page['sliders']=SliderModel::query()->orderBy('sort_order','asc')->get();
    }
}
