<?php namespace Boromir\Repair\Models;

use Model;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;
use System\Models\File;

/**
 * Model
 */
class DeviceType extends Model
{
    use Validation;
    use Sortable;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'boromir_repair_device_types';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachOne=[
        'servicetabimage'=>File::class,
        'servicetabimagemob'=>File::class
    ];
}
