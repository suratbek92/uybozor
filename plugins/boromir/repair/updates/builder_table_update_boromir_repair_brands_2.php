<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairBrands2 extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_brands', function($table)
        {
            $table->integer('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_brands', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
