<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairDeviceTypes7 extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_device_types', function($table)
        {
            $table->renameColumn('servicetab', 'uniquetext');
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_device_types', function($table)
        {
            $table->renameColumn('uniquetext', 'servicetab');
        });
    }
}
