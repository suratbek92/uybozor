<?php namespace Boromir\Repair\Models;

use Model;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;
use Boromir\Repair\Models\DeviceType;

/**
 * Model
 */
class Brand extends Model
{
    use Validation;
    use Sortable;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'boromir_repair_brands';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $hasMany = [
        'devicemodelcategories' => DeviceModelCategory::class,
        'devicemodels'          => DeviceModel::class,
    ];


    public function scopeGetByDeviceType($query)
    {
        $devicetypes  = DeviceType::query()->lists('id');
        $devicemodels = DeviceModel::whereIn('device_type_id', $devicetypes)
                                   ->get()
                                   ->reject(function ($devicemodel) {
                                       return empty($devicemodel->brand_id);
                                   });

        return $query->whereIn('id', $devicemodels->pluck('brand_id')->toArray())
                     ->get()
                     ->groupBy(function ($item) use ($devicemodels) {
                         return $devicemodels->keyBy('brand_id')->toArray()[$item['id']]['device_type_id'];

                     });
    }
}
