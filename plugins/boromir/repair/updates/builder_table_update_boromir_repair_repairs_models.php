<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairRepairsModels extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_repairs_models', function($table)
        {
            $table->renameColumn('model_id', 'device_model_id');
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_repairs_models', function($table)
        {
            $table->renameColumn('device_model_id', 'model_id');
        });
    }
}
