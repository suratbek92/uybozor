<?php namespace Boromir\Repair\Models;

use Boromir\Repair\Controllers\Repairs;
use Model;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;
use System\Models\File;

/**
 * Model
 */
class DeviceModel extends Model
{
    use Validation;
    use Sortable;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'boromir_repair_models';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $belongsTo=[
      'device_type'=>DeviceType::class
    ];
    public $belongsToMany = [
        //'device_type' => DeviceType::class,
        'repairs'     => [
            Repair::class,
            'table'    => 'boromir_repair_repairs_models',
            'key'      => 'device_model_id',
            'otherkey' => 'repair_id',
        ]
    ];
    public $attachOne=[
        'image'=>File::class
    ];

}
