<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairModels3 extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_models', function($table)
        {
            $table->renameColumn('category_id', 'device_model_category_id');
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_models', function($table)
        {
            $table->renameColumn('device_model_category_id', 'category_id');
        });
    }
}
