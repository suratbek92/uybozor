<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairDeviceTypes2 extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_device_types', function($table)
        {
            $table->integer('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_device_types', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
