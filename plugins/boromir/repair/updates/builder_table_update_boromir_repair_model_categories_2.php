<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairModelCategories2 extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_model_categories', function($table)
        {
            $table->integer('brand_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_model_categories', function($table)
        {
            $table->dropColumn('brand_id');
        });
    }
}
