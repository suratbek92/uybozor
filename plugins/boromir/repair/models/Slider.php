<?php namespace Boromir\Repair\Models;

use Model;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;
use System\Models\File;

/**
 * Model
 */
class Slider extends Model
{
    use Validation;
    use Sortable;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'boromir_repair_sliders';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachOne=[
        'image'=>File::class
    ];
}
