<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairBrands extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_brands', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_brands', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
