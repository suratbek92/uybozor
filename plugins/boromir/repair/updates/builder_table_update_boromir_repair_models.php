<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairModels extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_models', function($table)
        {
            $table->integer('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_models', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
