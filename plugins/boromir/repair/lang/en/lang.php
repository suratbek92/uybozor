<?php return [
    'device.manage' => 'Device Manage',
    'Brand name' => 'brandname',
    'brandname' => 'Brand name',
    'device_manage' => 'Device manage',
    'brand_manage' => 'Brand manage',
    'plugin' => [
        'name' => 'Repair',
    ],
    'brands' => 'Brands',
    'model_manage' => 'Model manage',
];