<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBoromirRepairSliders extends Migration
{
    public function up()
    {
        Schema::create('boromir_repair_sliders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('title');
            $table->string('tagline');
            $table->string('link');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('boromir_repair_sliders');
    }
}
