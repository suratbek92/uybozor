<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairModels6 extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_models', function($table)
        {
            $table->string('sort_order', 10)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_models', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
