<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBoromirRepairRepairs extends Migration
{
    public function up()
    {
        Schema::create('boromir_repair_repairs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('status')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('boromir_repair_repairs');
    }
}
