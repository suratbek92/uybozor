<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBoromirRepairRepairsModels2 extends Migration
{
    public function up()
    {
        Schema::create('boromir_repair_repairs_models', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('repair_id')->unsigned();
            $table->integer('model_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('boromir_repair_repairs_models');
    }
}
