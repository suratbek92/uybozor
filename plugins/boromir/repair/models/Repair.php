<?php namespace Boromir\Repair\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use System\Models\File;

/**
 * Model
 */
class Repair extends Model
{
    use Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'boromir_repair_repairs';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $belongsToMany=[
        'devicemodels'=>[
            DeviceModel::class,
            'table'=>'boromir_repair_repairs_models',
            'key'=>'repair_id',
            'otherkey'=>'device_model_id']
        ];
    public $attachOne=[
        'image'=>File::class
    ];
}
