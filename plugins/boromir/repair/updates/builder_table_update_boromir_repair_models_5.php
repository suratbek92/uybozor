<?php namespace Boromir\Repair\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBoromirRepairModels5 extends Migration
{
    public function up()
    {
        Schema::table('boromir_repair_models', function($table)
        {
            $table->integer('device_type_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('boromir_repair_models', function($table)
        {
            $table->dropColumn('device_type_id');
        });
    }
}
